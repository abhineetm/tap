//
//  TapViewController.m
//  Tap
//
//  Created by Abhineet on 28/07/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#define visibleTimeOut 0.5
#define hideTimeOut 0.5
#import "TapViewController.h"
#include <stdlib.h>

@implementation TapViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void)addButtons
{
	 rows = self.view.frame.size.height/40;
	 cols = self.view.frame.size.width/40;
	int tag = 1001;
	for(int i = 0 ; i<rows ; i++)
	{
		for(int j=0;j<cols;j++)
		{
			UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect]; 
			button.frame = CGRectMake(j*40, i*40, 40, 40);
			NSLog(@"%d",tag);
			button.tag = tag++;
			button.hidden = YES;
			//[button setImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
			
			[button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
			[self.view addSubview:button];
		}
	}
	
	colors = [NSArray arrayWithObjects:[UIColor blackColor],[UIColor grayColor],[UIColor redColor],[UIColor yellowColor],[UIColor brownColor],nil];
	totalCols = [colors count];
	currCol = 0;
	[self.view setBackgroundColor:[colors objectAtIndex:currCol]];
}

-(void)shuffle
{
	int nxtRow = arc4random()%rows;
	int nxtCol = arc4random()%cols;
	int tag = 1000 + (nxtRow * cols)+nxtCol;
	[((UIButton *)[self.view viewWithTag:tag]) setTitle:[NSString stringWithFormat:@"%d",score] forState:UIControlStateNormal];
	
	currPoint = CGPointMake( ((UIButton *)[self.view viewWithTag:tag]).center.x, ((UIButton *)[self.view viewWithTag:tag]).center.y);
	[self.view viewWithTag:tag].hidden=NO;
	tm = [NSTimer scheduledTimerWithTimeInterval:hti target:self selector:@selector(hideButtonWithTag:) userInfo:[self.view viewWithTag:tag] repeats:NO];
	
}

-(void)hideButtonWithTag:(NSTimer *)tmr
{
	UIButton *hideButton = (UIButton *)[tmr userInfo];
	hideButton.hidden =YES;
	prevPoint = CGPointMake(hideButton.center.x, hideButton.center.y);
	//[self.view viewWithTag:tag].hidden = YES;
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	score = 0;
	vti = 0.2;
	hti = 0.6;
	[self addButtons];
	timer = [NSTimer scheduledTimerWithTimeInterval:vti target:self selector:@selector(shuffle) userInfo:nil repeats:YES];
	[super viewDidLoad];

}

-(IBAction)buttonTapped:(id)sender
{
	score++;
	/*if(score%3 == 0)
	{
		vti-=0.2;
		hti-=0.2;
	}*/
	[((UIButton *)sender) setTitle:[NSString stringWithFormat:@"%d",score] forState:UIControlStateSelected];
	tm = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(strike:) userInfo:[NSNumber numberWithInt:1] repeats:NO];


	
}

-(void)strike:(NSTimer *)t
{
	switch ([[t userInfo]intValue]) {
		case 1:
		{
			[self.view setBackgroundColor:[UIColor redColor]];
			tm = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(strike:) userInfo:[NSNumber numberWithInt:2] repeats:NO];
		}
		break;
		case 2:
		{
			[self.view setBackgroundColor:[UIColor yellowColor]];
			tm = [NSTimer scheduledTimerWithTimeInterval:0.06 target:self selector:@selector(strike:) userInfo:[NSNumber numberWithInt:3] repeats:NO];
		}
			break;
		case 3:
		{
			[self.view setBackgroundColor:[UIColor blackColor]];
			//NSTimer *tm = [NSTimer scheduledTimerWithTimeInterval:0.06 target:self selector:@selector(strike:) userInfo:[NSNumber numberWithInt:3] repeats:NO];
		}
			break;
			
		default:
			break;
	}
}

-(void)drawInRect:(CGRect)rect
{
	CGContextRef context    = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    CGContextSetLineWidth(context, 2.0);
    CGContextMoveToPoint(context, prevPoint.x,prevPoint.y); //start at this point
	CGContextAddLineToPoint(context,currPoint.x,currPoint.y); //draw to this point
	CGContextStrokePath(context);
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
