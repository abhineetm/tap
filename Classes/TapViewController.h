//
//  TapViewController.h
//  Tap
//
//  Created by Abhineet on 28/07/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TapViewController : UIViewController {

	NSTimer *timer;
	NSTimer *tm;
	int rows;
	int cols;
	NSArray *colors;
	NSInteger currCol;
	NSInteger totalCols;
	int score;
	double vti;
	double hti;
	int scnt;
	CGPoint prevPoint;
	CGPoint currPoint;

}

@end

