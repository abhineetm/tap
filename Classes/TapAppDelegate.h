//
//  TapAppDelegate.h
//  Tap
//
//  Created by Abhineet on 28/07/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TapViewController;

@interface TapAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    TapViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TapViewController *viewController;

@end

